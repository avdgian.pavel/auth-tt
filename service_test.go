package cerberus

import (
	"testing"

	"gopkg.in/oauth2.v3/models"
)

type stubClientRepo struct{}

var validClientId = "valid-id"

func (r *stubClientRepo) Create(c *Client) error {
	return nil
}

func (r *stubClientRepo) GetByID(id string) (*Client, error) {
	if id == validClientId {
		return &Client{
			models.Client{
				ID:     id,
				Secret: "test",
				Domain: "test.test",
			},
		}, nil
	} else {
		return nil, ErrClientNotFound
	}
}

func TestCreateClient(t *testing.T) {
	service := NewClientService(&stubClientRepo{})
	domain := "test.test"
	cl, err := service.CreateClient(domain)
	if err != nil {
		t.Log("error on service.CreateClient call")
		t.Fail()
	}
	if cl == nil {
		t.Log("expected an instance of a Client")
		t.Fail()
	} else if cl.GetDomain() != domain {
		t.Logf("expected domain: %s, got: %s", domain, cl.GetDomain())
		t.Fail()
	}
}

func TestFindClient(t *testing.T) {
	service := NewClientService(&stubClientRepo{})
	if _, err := service.GetClient(validClientId); err != nil {
		t.Log("expected client instance")
		t.Fail()
	}
}
