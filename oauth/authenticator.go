package oauth

import (
	"gitlab.com/avdgian.pavel/cerberus"
	"gitlab.com/avdgian.pavel/cerberus/util"
	"gopkg.in/oauth2.v3/server"
)

type OauthAuthenticator struct {
	srv     *server.Server
	service cerberus.ClientService
	hosts   []string
}

func NewOauthAuthenticator(
	srv *server.Server,
	service cerberus.ClientService,
	hosts []string,
) *OauthAuthenticator {
	return &OauthAuthenticator{
		srv,
		service,
		hosts,
	}
}

func (a *OauthAuthenticator) Authenticate(tk cerberus.Token) (*cerberus.Client, error) {
	ti, err := a.srv.Manager.LoadAccessToken(tk.Val)
	if err != nil {
		return nil, err
	}

	cl, err := a.service.GetClient(ti.GetClientID())
	if err != nil {
		return nil, err
	}

	return cl, nil
}

func (a *OauthAuthenticator) Supports(tk cerberus.Token, host string) bool {
	return util.SliceContains(a.hosts, host) && cerberus.TokenTypeBearer == tk.Type
}
