package cerberus

import "testing"

type stubAuthenticator struct{}

func (a *stubAuthenticator) Authenticate(tk Token) (*Client, error) {
	if tk.Val == "valid-token" {
		return &Client{}, nil
	} else {
		return nil, ErrTokenNotSupported
	}
}

func (a *stubAuthenticator) Supports(tk Token, host string) bool {
	return host == "test.test"
}

func TestAuthenticatorValidData(t *testing.T) {
	authStrategy := &stubAuthenticator{}
	authenticator := NewAuthenticator([]AuthenticationStrategy{authStrategy})

	token := NewToken(TokenTypeBearer, "valid-token")
	host := "test.test"

	cl, err := authenticator.Authenticate(token, host)
	if cl == nil || err != nil {
		t.Error("expected to get client instance")
		t.Fail()
	}
}

func TestAuthenticatorInvalidHost(t *testing.T) {
	authStrategy := &stubAuthenticator{}
	authenticator := NewAuthenticator([]AuthenticationStrategy{authStrategy})

	token := NewToken(TokenTypeBearer, "valid-token")
	host := "nottest.test"

	_, err := authenticator.Authenticate(token, host)
	if err == nil {
		t.Errorf("expected to receive error: %s\n", ErrTokenNotSupported.Error())
		t.Fail()
	}
}

func TestAuthenticatorInvalidToken(t *testing.T) {
	authStrategy := &stubAuthenticator{}
	authenticator := NewAuthenticator([]AuthenticationStrategy{authStrategy})

	token := NewToken(TokenTypeBearer, "invalid-token")
	host := "test.test"

	_, err := authenticator.Authenticate(token, host)
	if err == nil {
		t.Errorf("expected to receive error: %s\n", ErrTokenNotSupported.Error())
		t.Fail()
	}
}
