package cerberus

import "testing"

func TestNewClient(t *testing.T) {
	host := "test.test"

	client := NewClient(host)
	if client.GetDomain() != host {
		t.Logf("invalid domain value. Expected %s, got %s\n", host, client.GetDomain())
		t.Fail()
	}
	if client.GetID() == "" {
		t.Log("client id is empty")
		t.Fail()
	}
	if client.GetSecret() == "" {
		t.Log("client secret is empty")
		t.Fail()
	}
}
