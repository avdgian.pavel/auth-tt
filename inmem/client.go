package inmem

import (
	"gitlab.com/avdgian.pavel/cerberus"
)

type InMemoryClientRepository struct {
	clients map[string]*cerberus.Client
}

func NewInMemoryClientRepository() *InMemoryClientRepository {
	return &InMemoryClientRepository{
		clients: make(map[string]*cerberus.Client),
	}
}

func (r *InMemoryClientRepository) GetByID(id string) (*cerberus.Client, error) {
	c, found := r.clients[id]
	if !found {
		return nil, cerberus.ErrClientNotFound
	}

	return c, nil
}

func (r *InMemoryClientRepository) Create(c *cerberus.Client) error {
	r.clients[c.GetID()] = c
	return nil
}
