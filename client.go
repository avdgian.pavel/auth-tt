package cerberus

import (
	"errors"

	"github.com/google/uuid"
	"gopkg.in/oauth2.v3/models"
)

type Client struct {
	models.Client
}

var ErrClientNotFound = errors.New("client not found")

type ClientRepository interface {
	GetByID(id string) (*Client, error)
	Create(c *Client) error
}

func NewClient(domain string) *Client {
	clientId := uuid.New().String()[:8]
	clientSecret := uuid.New().String()[:8]

	return &Client{
		models.Client{
			ID:     clientId,
			Secret: clientSecret,
			Domain: domain,
		},
	}
}
