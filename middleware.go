package cerberus

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/go-kit/kit/endpoint"
	httptransport "github.com/go-kit/kit/transport/http"
)

var ErrUnauthorized = errors.New("unauthorized")

type AuthFunc func(tk Token, host string) (*Client, error)

func NewTokenValidator(authFunc AuthFunc) endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (response interface{}, err error) {
			auth := ctx.Value(httptransport.ContextKeyRequestAuthorization).(string)
			host := ctx.Value(httptransport.ContextKeyRequestHost).(string)
			tk, err := parseAuthorizationHeader(auth)
			if err != nil {
				fmt.Printf("error parsing token: %s\n", err.Error())
				return nil, ErrUnauthorized
			}

			cl, err := authFunc(tk, host)
			if err != nil {
				fmt.Printf("error authenticating token: %s\n", err.Error())
				return nil, ErrUnauthorized
			}
			ctx = context.WithValue(ctx, AuthenticatedClientKey, cl)

			return next(ctx, request)
		}
	}
}

func parseAuthorizationHeader(auth string) (Token, error) {
	bearerPrefix := "Bearer "
	basicPrefix := "Basic "
	prefix := ""
	tokType := ""

	if strings.HasPrefix(auth, bearerPrefix) {
		prefix = bearerPrefix
		tokType = TokenTypeBearer
	} else if strings.HasPrefix(auth, basicPrefix) {
		prefix = basicPrefix
		tokType = TokenTypeBasic
	}

	var token Token
	if auth != "" && prefix != "" {
		token = NewToken(tokType, auth[len(prefix):])
		return token, nil
	} else {
		return token, fmt.Errorf("failed to parse authorization header: %s", auth)
	}
}
