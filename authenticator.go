package cerberus

import (
	"errors"
)

type clientKey string

const AuthenticatedClientKey clientKey = "client"
const (
	TokenTypeBearer = "Bearer"
	TokenTypeBasic  = "Basic"
)

type Token struct {
	Type string
	Val  string
}

var ErrTokenNotSupported = errors.New("provided token is not supported")

type AuthenticationStrategy interface {
	Authenticate(tk Token) (*Client, error)
	Supports(tk Token, host string) bool
}

type Authenticator struct {
	strategies []AuthenticationStrategy
}

func NewToken(tokType string, tokVal string) Token {
	return Token{
		tokType,
		tokVal,
	}
}

func NewAuthenticator(strategies []AuthenticationStrategy) *Authenticator {
	return &Authenticator{
		strategies,
	}
}

func (a *Authenticator) Authenticate(tk Token, host string) (*Client, error) {
	for _, s := range a.strategies {
		if s.Supports(tk, host) {
			return s.Authenticate(tk)
		}
	}
	return nil, ErrTokenNotSupported
}
