package basic

import (
	"encoding/base64"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/avdgian.pavel/cerberus"
	"gitlab.com/avdgian.pavel/cerberus/util"
)

var ErrMissingToken = errors.New("token value missing")
var ErrInvalidToken = errors.New("provided token is invalid")

type BasicAuthAuthenticator struct {
	service cerberus.ClientService
	hosts   []string
}

func NewBasicAuthAuthenticator(service cerberus.ClientService, hosts []string) *BasicAuthAuthenticator {
	return &BasicAuthAuthenticator{
		service,
		hosts,
	}
}

func (a *BasicAuthAuthenticator) Authenticate(tk cerberus.Token) (*cerberus.Client, error) {
	token := tk.Val
	credsStr, err := base64.StdEncoding.DecodeString(token)
	if err != nil {
		return nil, fmt.Errorf("failed to decode token %s: %w", token, err)
	}

	creds := strings.Split(string(credsStr), ":")
	if len(creds) != 2 {
		return nil, ErrInvalidToken
	}

	clientID, clientSecret := creds[0], creds[1]
	client, err := a.service.GetClient(clientID)
	if err != nil {
		return nil, err
	}
	if client.GetSecret() != clientSecret {
		return nil, ErrInvalidToken
	}

	return client, nil
}

func (a *BasicAuthAuthenticator) Supports(tk cerberus.Token, host string) bool {
	return util.SliceContains(a.hosts, host) && cerberus.TokenTypeBasic == tk.Type
}
